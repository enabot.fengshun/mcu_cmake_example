/******************************************************************************
 * Copyright (C) 2017, Huada Semiconductor Co.,Ltd All rights reserved.
 *
 * This software is owned and published by:
 * Huada Semiconductor Co.,Ltd ("HDSC").
 *
 * BY DOWNLOADING, INSTALLING OR USING THIS SOFTWARE, YOU AGREE TO BE BOUND
 * BY ALL THE TERMS AND CONDITIONS OF THIS AGREEMENT.
 *
 * This software contains source code for use with HDSC
 * components. This software is licensed by HDSC to be adapted only
 * for use in systems utilizing HDSC components. HDSC shall not be
 * responsible for misuse or illegal use of this software for devices not
 * supported herein. HDSC is providing this software "AS IS" and will
 * not be responsible for issues arising from incorrect user implementation
 * of the software.
 *
 * Disclaimer:
 * HDSC MAKES NO WARRANTY, EXPRESS OR IMPLIED, ARISING BY LAW OR OTHERWISE,
 * REGARDING THE SOFTWARE (INCLUDING ANY ACOOMPANYING WRITTEN MATERIALS),
 * ITS PERFORMANCE OR SUITABILITY FOR YOUR INTENDED USE, INCLUDING,
 * WITHOUT LIMITATION, THE IMPLIED WARRANTY OF MERCHANTABILITY, THE IMPLIED
 * WARRANTY OF FITNESS FOR A PARTICULAR PURPOSE OR USE, AND THE IMPLIED
 * WARRANTY OF NONINFRINGEMENT.
 * HDSC SHALL HAVE NO LIABILITY (WHETHER IN CONTRACT, WARRANTY, TORT,
 * NEGLIGENCE OR OTHERWISE) FOR ANY DAMAGES WHATSOEVER (INCLUDING, WITHOUT
 * LIMITATION, DAMAGES FOR LOSS OF BUSINESS PROFITS, BUSINESS INTERRUPTION,
 * LOSS OF BUSINESS INFORMATION, OR OTHER PECUNIARY LOSS) ARISING FROM USE OR
 * INABILITY TO USE THE SOFTWARE, INCLUDING, WITHOUT LIMITATION, ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL OR CONSEQUENTIAL DAMAGES OR LOSS OF DATA,
 * SAVINGS OR PROFITS,
 * EVEN IF Disclaimer HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 * YOU ASSUME ALL RESPONSIBILITIES FOR SELECTION OF THE SOFTWARE TO ACHIEVE YOUR
 * INTENDED RESULTS, AND FOR THE INSTALLATION OF, USE OF, AND RESULTS OBTAINED
 * FROM, THE SOFTWARE.
 *
 * This software may be replicated in part or whole for the licensed use,
 * with the restriction that this Disclaimer and Copyright notice must be
 * included with each copy of this software, whether used in part or whole,
 * at all times.
 */

#include "ddl.h"
#include "fifo.h"
#include "flash.h"
#include "gpio.h"
#include "sysctrl.h"
#include "uart.h"
#include "uart_interface.h"
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>

//时钟相关配置: 配置PLL时钟48MHz
void ClockConfig(void) {
  stc_sysctrl_pll_cfg_t stcPLLCfg;

  Sysctrl_SetRCLTrim(SysctrlRclFreq32768);
  Sysctrl_SetRCLStableTime(SysctrlRclStableCycle64);
  Sysctrl_ClkSourceEnable(SysctrlClkRCL, TRUE);
  Sysctrl_SysClkSwitch(SysctrlClkRCL);

  Sysctrl_SetRCHTrim(SysctrlRchFreq4MHz);
  Sysctrl_ClkSourceEnable(SysctrlClkRCH, TRUE);

  stcPLLCfg.enInFreq = SysctrlPllInFreq4_6MHz;     // RCH 4MHz
  stcPLLCfg.enOutFreq = SysctrlPllOutFreq36_48MHz; // PLL
  stcPLLCfg.enPllClkSrc = SysctrlPllRch;           // clock source = RCH
  stcPLLCfg.enPllMul = SysctrlPllMul12;            // 4MHz x 12 = 48MHz
  Sysctrl_SetPLLFreq(&stcPLLCfg);

  //  when HCLK > 24M : set flash read wait time with  1 cycle
  Flash_WaitCycle(FlashWaitCycle1);

  stc_sysctrl_clk_cfg_t stcCfg;
  ///< 选择PLL作为HCLK时钟源;
  stcCfg.enClkSrc = SysctrlClkPLL;
  ///< HCLK SYSCLK/2
  stcCfg.enHClkDiv = SysctrlHclkDiv1;
  ///< PCLK 为HCLK/8
  stcCfg.enPClkDiv = SysctrlPclkDiv1;
  ///< 系统时钟初始化
  Sysctrl_ClkInit(&stcCfg);
}

volatile uint64_t time_tick_ms;
extern "C" void SysTick_IRQHandler() { ++time_tick_ms; }

void msleep(uint32_t msec) {
  int start = time_tick_ms;
  while (time_tick_ms - start < msec)
    ;
}

char debug_str[256];

void debug(const char *format, ...) {
  va_list va;
  va_start(va, format);
  vsnprintf(debug_str, sizeof(debug_str), format, va);
  va_end(va);
  UartWrite(debug_str, strlen(debug_str));
}

/**
 ******************************************************************************
 ** \brief  Main function of project
 **
 ** \return uint32_t return value, if needed
 **
 ** This sample
 **
 ******************************************************************************/
int main() {
  ClockConfig();
  UartOpen(115200);

  ///< 内核函数，SysTick配置，定时1s，系统时钟默认RCH 4MHz
  SysTick_Config(SystemCoreClock / 1000);

#pragma clang diagnostic push
#pragma ide diagnostic ignored "EndlessLoop"
  int times = 0;
  while (true) {
    char out_buf[65];
    memset(out_buf, 0, sizeof(out_buf));
    auto num = UartRead(out_buf, sizeof(out_buf) - 1);
    if (!num) {
      char c = (++times) % 10 + '0';
      UartWrite(&c, 1);
      const char str[] = "Wait input.\r\n";
      UartWrite(str, strlen(str));
      msleep(1000);
      continue;
    }
    debug("received: %d [%s]\r\n", num, out_buf);
  }
#pragma clang diagnostic pop
}
