#include "uart_interface.h"
#include "fifo.h"

static char rx_fifo_buffer[512];
static FifoPowerOfTwo rx_fifo(rx_fifo_buffer, sizeof(rx_fifo_buffer));

static bool initialized = false;

static void PortInit(void) {
  stc_gpio_cfg_t stcGpioCfg;

  DDL_ZERO_STRUCT(stcGpioCfg);

  Sysctrl_SetPeripheralGate(SysctrlPeripheralGpio,
                            TRUE); // GPIO外设模块时钟使能

  stcGpioCfg.enDir = GpioDirOut;
  Gpio_Init(GpioPortA, GpioPin9, &stcGpioCfg);
  Gpio_SetAfMode(GpioPortA, GpioPin9, GpioAf1); //配置PA09 为UART0 TX
  stcGpioCfg.enDir = GpioDirIn;
  Gpio_Init(GpioPortA, GpioPin10, &stcGpioCfg);
  Gpio_SetAfMode(GpioPortA, GpioPin10, GpioAf1); //配置PA10 为UART0 RX
}

//串口模块配置
static void UartCfg(uint32_t baud) {
  stc_uart_cfg_t stcCfg;

  DDL_ZERO_STRUCT(stcCfg);

  Sysctrl_SetPeripheralGate(SysctrlPeripheralUart0,
                            TRUE); // UART0外设模块时钟使能

  stcCfg.enRunMode = UartMskMode1;           //模式1
  stcCfg.enStopBit = UartMsk1bit;            // 1位停止位
  stcCfg.enMmdorCk = UartMskDataOrAddr;      //无校验
  stcCfg.stcBaud.u32Baud = baud;             //波特率
  stcCfg.stcBaud.enClkDiv = UartMsk8Or16Div; //通道采样分频配置
  stcCfg.stcBaud.u32Pclk = Sysctrl_GetPClkFreq(); //获得外设时钟（PCLK）频率值
  Uart_Init(M0P_UART0, &stcCfg);                  //串口初始化

  Uart_ClrStatus(M0P_UART0, UartRC);    //清接收请求
  Uart_ClrStatus(M0P_UART0, UartTC);    //清发送请求
  Uart_EnableIrq(M0P_UART0, UartRxIrq); //使能串口接收中断
  //  Uart_EnableIrq(M0P_UART0, UartTxIrq);    //使能串口发送中断
  EnableNvic(UART0_IRQn, IrqLevel3, TRUE); ///<系统中断使能
}

int UartOpen(uint32_t baud) {
  if (!initialized) {
    PortInit();
    UartCfg(baud);
    initialized = true;
  }
  return 0;
}

int UartWrite(const void *buff, size_t len) {
  if (!buff)
    return 0;

  for (int i = 0; i < len; ++i) {
    Uart_SendDataPoll(M0P_UART0, *((uint8_t *)buff + i)); //查询方式发送数据
  }
  return len;
}

int UartRead(void *out_buf, size_t num_elements) {
  return rx_fifo.Out(out_buf, num_elements);
}

int UartAvailable() { return rx_fifo.used(); }

// UART0中断函数
extern "C" void Uart0_IRQHandler(void) {
  if (Uart_GetStatus(M0P_UART0, UartRC)) // UART0数据接收
  {
    Uart_ClrStatus(M0P_UART0, UartRC);    //清中断状态位
    char c = Uart_ReceiveData(M0P_UART0); //接收数据字节
    rx_fifo.In(&c, 1);
  }
}
