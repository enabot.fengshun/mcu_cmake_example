//
// Created by fs on 2020-11-04.
//

#ifndef MCU_CMAKE_EXAMPLE_SRC_UART_INTERFACE_H_
#define MCU_CMAKE_EXAMPLE_SRC_UART_INTERFACE_H_

#include "ddl.h"
#include "gpio.h"
#include "sysctrl.h"
#include "uart.h"

#ifdef __cplusplus
extern "C" {
#endif

int UartOpen(uint32_t baud);
int UartRead(void *out_buf, size_t num_elements);
int UartWrite(const void *buff, size_t len);
int UartAvailable();

#ifdef __cplusplus
}
#endif

#endif // MCU_CMAKE_EXAMPLE_SRC_UART_INTERFACE_H_
